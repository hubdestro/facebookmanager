package com.facebookintegration.model;


import android.net.Uri;

/**
 * Created by root on 12/5/16
 */
public class User {

    public String id;
    public String name;
    public String first_name;
    public String last_name;
    public String gender;
    public Uri picture;
    public String email;
    public FriendsList invitable_friends;
}
