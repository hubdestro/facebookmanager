package com.facebookintegration.socialhelper;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.widget.ShareDialog;
import com.facebookintegration.utils.Utils;
import com.facebookintegration.model.User;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.FacebookCallback;
import com.facebook.CallbackManager;
import com.facebook.GraphResponse;
import com.facebook.GraphRequest;
import com.facebook.share.Sharer;
import com.facebook.FacebookSdk;
import android.app.Application;
import android.content.Context;
import com.facebook.Profile;
import com.google.gson.Gson;
import android.app.Activity;
import org.json.JSONObject;
import android.os.Bundle;
import android.net.Uri;

/**
 * Created by Ankit Demonstrate on 17/5/16
 * My Custom Facebook Login Manager
 * Steps : -
 *  1) call getInstance() and store class Object
 *  2) call initialiseSdk(with parameter) before setContentView() and set Permission whatever you want to access
 *  3) create Model named as User { put fields whichever you want}
 *  4) implement SocialCallBack you will get result over there
 *  5) note should call getCallbackManager().onActivityResult(param) in onActivityResult(param)
 */
public class FacebookLogin {

    private static FacebookLogin facebookLogin;
    private User user;
    private SocialCallback fbCallback;
    private String permission;
    private Context context;

    /***
     * private constructor to initialize basic things which required for login into facebook
     */
    private FacebookLogin() {
    }

    /***
     * getInstance method to archive singleton pattern
     * @return this Object  if it is not already created
     */
    public static synchronized FacebookLogin getInstance() {
        if (facebookLogin == null ){
            facebookLogin = new FacebookLogin();
            return facebookLogin;
        } else {
            return facebookLogin;
        }
    }

    /***
     * should called fist after object creation
     * @param context current context of application
     * @param application object of application
     * @param reqCode request code to differentiate the request
     */
    public void initialiseSdk(Context context, Application application, int reqCode) {
        this.context = context;
        FacebookSdk.sdkInitialize(context.getApplicationContext(), reqCode);
        AppEventsLogger.activateApp(application); //AppEventsLogger.activateApp(this) is deprecated
        fbCallback = (SocialCallback)context;
    }

    /***
     * to get back data from popup to models
     * @return CallBack Object
     */
    public CallbackManager getCallbackManager() {
        CallbackManager callbackManager = CallbackManager.Factory.create();
        handleRequest(callbackManager);
        return callbackManager;
    }

    /***
     * fb require permission for few fields
     * @param permission receive quoted String of permission
     */
    public void setPermission(String permission) {
        this.permission = permission;
    }

    /***
     * facebook login
     * with out using login button , will work on your login button
     * @param callbackManager receive callback object
     */

    private void handleRequest(CallbackManager callbackManager) {
        final LoginManager loginManager = LoginManager.getInstance();
        loginManager.setLoginBehavior(LoginBehavior.DEVICE_AUTH);
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Utils.d("inside onSuccess()");
                GraphRequest userRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Utils.d(object+"END OF OBJECT DATA");
                            Gson gson = new Gson();
                            user = gson.fromJson(object.toString(),User.class);
                            Profile profile = Profile.getCurrentProfile();
                            user.picture = profile.getProfilePictureUri(50,50);
                            fbCallback.getFbUserDetail(user);
                        } catch (Exception e) {
                            Utils.d(e.toString());
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", permission);
                userRequest.setParameters(parameters);
                userRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                fbCallback.getFbUserDetail(user);
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }

    /***
     * Login by using login button
     * @param loginButton loginButton
     * @param callbackManager Callback Manager
     */
    private void handleRequest(LoginButton loginButton, CallbackManager callbackManager) {
        loginButton.setReadPermissions();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Utils.d("inside onSuccess()");
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Utils.d(object+"");
                            Gson gson = new Gson();
                            fbCallback.getFbUserDetail(gson.fromJson(object.toString(),User.class));
                        } catch (Exception e) {
                            Utils.d(e.toString());
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", permission);
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                fbCallback.getFbUserDetail(user);
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }


    /***
     * will pop up post dialog windows
     */
    public void postStatusOnWall() {
        ShareDialog shareDialog = new ShareDialog((Activity) context);
        shareDialog.registerCallback(getCallbackManager(), new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Utils.d("inside onSuccess()");
            }

            @Override
            public void onCancel() {
                Utils.d("inside onCancel()");
            }

            @Override
            public void onError(FacebookException error) {
                Utils.d("inside onError()");
            }
        });

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Hubdestro Labs")
                    .setContentDescription("This status is a test Post, please ignore it we are just testing our implementation")
                    .setContentUrl(Uri.parse("http://www.hubdestro.blogspot.in"))
                    .build();
            shareDialog.show(linkContent);

        }
    }
}
