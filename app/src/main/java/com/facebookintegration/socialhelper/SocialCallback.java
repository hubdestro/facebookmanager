package com.facebookintegration.socialhelper;


import com.facebookintegration.model.User;

/**
 * Created by root on 17/5/16
 */
public interface SocialCallback {
    /* get user detail from facebook */
    void getFbUserDetail(User user);
}
