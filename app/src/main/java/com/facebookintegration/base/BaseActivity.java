package com.facebookintegration.base;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Demonstrate
 * Base Activity for all activity
 */
public abstract class BaseActivity extends AppCompatActivity {

    /**
     * @see Override this method to set widgets reference
     */
    public abstract void setWidgetReferences();

    /**
     * @see Override this method to register widgets to there listener
     */
    public abstract void setWidgetListener();
}
