package com.facebookintegration.activities.facebook;

import com.facebookintegration.socialhelper.SocialCallback;
import android.support.design.widget.FloatingActionButton;
import com.facebookintegration.socialhelper.FacebookLogin;
import com.facebookintegration.base.BaseActivity;
import android.support.design.widget.Snackbar;
import com.facebook.login.widget.LoginButton;
import com.facebookintegration.utils.Utils;
import com.facebookintegration.model.User;
import android.support.v7.widget.Toolbar;
import com.squareup.picasso.Picasso;
import com.facebookintegration.R;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.content.Intent;
import android.view.MenuItem;
import android.widget.Button;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;

/***
 * Activity used to login ino facebook
 */
public class FacebookActivity extends BaseActivity implements View.OnClickListener, SocialCallback {

    private static final int RC_F_SIGN_IN = 101;
    private String permissionString = "id,name,email,invitable_friends";
    private FacebookLogin facebookLogin = FacebookLogin.getInstance();
    private LoginButton loginButton;
    private TextView txtName;
    private ImageView imgUser;
    private Button btnPost;
    private ListView friendListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookLogin.initialiseSdk(this,getApplication(), RC_F_SIGN_IN);
        facebookLogin.setPermission(permissionString);
        setContentView(R.layout.activity_main);
        setWidgetReferences();
        setWidgetListener();
        //handle Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //floating button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Option Disabled by Developer", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setWidgetReferences() {
        loginButton = (LoginButton) findViewById(R.id.loginButton);
        if (loginButton != null) {
            loginButton.setReadPermissions("public_profile,email,user_friends");
        }
        txtName = (TextView) findViewById(R.id.txtName);
        imgUser = (ImageView) findViewById(R.id.imgUser);
        btnPost = (Button) findViewById(R.id.btnPost);
        friendListView = (ListView) findViewById(R.id.friendListView);
    }

    @Override
    public void setWidgetListener() {
        btnPost.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        facebookLogin.postStatusOnWall();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Utils.d("inside onActivityResult()");
        facebookLogin.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void getFbUserDetail(User user) {
        Utils.d("inside getFbDetail()");
        Utils.d("Detail : "+user.name);
        Utils.d("Detail : "+user.email);
        Utils.d("Detail : "+user.invitable_friends.data.get(0).name);
        Picasso.with(this).load(user.picture).into(imgUser);
        txtName.setText(user.name);
        FriendsAdaptor adaptor = new FriendsAdaptor(this,user.invitable_friends.data);
        friendListView.setAdapter(adaptor);
    }
}
