package com.facebookintegration.activities.facebook;

import com.facebookintegration.model.FriendDetail;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.TextView;
import android.view.ViewGroup;
import java.util.ArrayList;
import android.view.View;

/**
 * Created by ankit on 20/6/16
 */
public class FriendsAdaptor extends BaseAdapter {

    private final Context context;
    private final ArrayList<FriendDetail> list;

    public FriendsAdaptor(Context context, ArrayList<FriendDetail> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1,null);
        }
        TextView text1 = (TextView) convertView.findViewById(android.R.id.text1);
        text1.setText(list.get(position).name);
        return convertView;
    }
}
