package com.facebookintegration.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Demonstrate on 11/4/16
 */
public class Utils {

    //fields declaration
    private static boolean MODE = true;

    public static void d(String msg) {
        if(MODE && msg != null) {
            Log.d(/*Thread.currentThread().getStackTrace()[3].getClassName()+"" +
                    " : "+*/Thread.currentThread().getStackTrace()[3].getMethodName(),msg);
        }
    }

    //static method -2
    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    //static method -3
    public static String getStringFrom(TextView textView) {
        return  textView != null ? textView.getText().toString().trim() : " ";
    }


    //static method -6
    public static String getDeviceId(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    //static method -10
    public static boolean checkPermission(final Context context, final String PERMISSION_STRING)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, PERMISSION_STRING) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, PERMISSION_STRING)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{PERMISSION_STRING}, 0);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{PERMISSION_STRING}, 0);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
