# README #


## Facebook Manager to integrate Facebook into your android application easily ##

* Demo(we are working in it)
* Version 1.0


### How do I get set up? ###

Steps to setup Facebook manager :

* Download and copy FacebookLogin, SocialCallBack and User Model into your       project(This is demo project we are working more on this)

* get the class object by calling getInstance() method, before setContentView() in onCreate().

* call initialiseSdk(params..)

* call getCallbackManger().onActivityResult(params...) in onActivityResult() method.

* get your user data into callback 

### Gradle Dependency ###
compile 'in.hubdestro:fbmanager:1.0'

### ADMIN ###

* Ankit Demonstrate